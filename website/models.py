from django.db import models

# Create your models here.
from crawling.KidneyOrgCrawler import KidneyOrgCrawler
from nlp.preprocessing import get_hotwords
from nlp.sentence import Sentence
from website.custom_fields import VectorField


class Keyword(models.Model):
    word = models.CharField(max_length=256, null=False, unique=True)

    @classmethod
    def create_if_not_exists(cls, word):
        if Keyword.objects.filter(word=word).exists():
            return Keyword.objects.get(word=word)
        else:
            new_keyword = Keyword(word=word)
            new_keyword.save()
            return new_keyword

    @classmethod
    def multiple_create_if_not_exist(cls, words):
        new_keywords = []
        for word in words:
            new_keywords.append(cls.create_if_not_exists(word))

        return new_keywords

    def __str__(self):
        return self.word


class Scraper(models.Model):
    name = models.CharField(max_length=255)
    link = models.URLField(unique=True)

    @classmethod
    def create_if_not_exists(cls, name, link):
        if Scraper.objects.filter(link=link).exists():
            return Scraper.objects.get(link=link)
        else:
            obj = cls()
            obj.link = link
            obj.name = name
            obj.save()

        return obj

    def to_crawler(self):
        if self.name == "KidneyOrg":
            return KidneyOrgCrawler()
        else:
            return None  # TODO: raise an exception


class ScrapedPost(models.Model):
    title = models.CharField(max_length=4069)
    content = models.CharField(max_length=4069, null=True)
    link = models.URLField(max_length=255, unique=True)
    author = models.CharField(max_length=255, null=True)
    publication_date = models.DateField(null=True)
    keywords = models.ManyToManyField(Keyword)

    @classmethod
    def create_if_not_exists(cls, title, content, link, author, keywords, publication_date):
        if ScrapedPost.objects.filter(link=link).exists():
            return ScrapedPost.objects.get(link=link)
        else:
            obj = cls()
            obj.title = title
            obj.content = content
            obj.link = link
            obj.author = author
            obj.publication_date = publication_date
            obj.save()

            obj.keywords.set(keywords)

            # TODO queue the embedding generation / import the embedding

            obj.save()

            return obj

    def __str__(self):
        return self.title


# TODO: fix naming inconsistency between scrape and crawl which i used interchangeably
class ScrapedQuery(models.Model):
    scraper = models.ForeignKey(Scraper, on_delete=models.CASCADE)
    keywords = models.ManyToManyField(Keyword)
    results = models.ManyToManyField(ScrapedPost)
    crawl_depth = models.PositiveSmallIntegerField()

    @classmethod
    def create_if_not_exists(cls, scraper, keywords, results, crawl_depth):
        db_keywords = Keyword.multiple_create_if_not_exist(keywords)

        existing_query = scraper.to_crawler().get_previous_query(keywords=keywords)
        if existing_query:
            existing_query.crawl_depth = crawl_depth
            existing_query.results.add(*results)
            existing_query.save()
            return existing_query

        obj = cls()
        obj.scraper = scraper
        obj.crawl_depth = crawl_depth
        obj.save()
        obj.results.set(results)
        obj.keywords.set(db_keywords)

        obj.save()

        return obj

    def update(self, new_crawl_depth, new_results):
        self.crawl_depth = new_crawl_depth
        self.results.add(*new_results)
        self.save()


MAX_KEYWORDS_PER_EMBEDDED_SENTENCE = 20


class EmbeddedSentence(models.Model):
    embedding_vector = VectorField()
    sentence = models.CharField(max_length=1024, unique=True)
    keywords = models.ManyToManyField(Keyword)
    containing_post = models.ForeignKey(ScrapedPost, on_delete=models.CASCADE)

    @classmethod
    def create_if_not_exists(cls, embedding_vector, sentence, keywords, containing_post):

        # Create the keywords if they don't exist yet
        saved_keywords = []
        for keyword in keywords:
            saved_keywords.append(Keyword.create_if_not_exists(keyword))

        if EmbeddedSentence.objects.filter(sentence=sentence).exists():
            existing_sentence = EmbeddedSentence.objects.get(sentence=sentence)

            # Add the keywords (maybe additional keywords can be associated with this sentence)
            existing_sentence.keywords.add(*saved_keywords)
            return existing_sentence
        else:
            obj = cls()
            obj.embedding_vector = embedding_vector
            obj.sentence = sentence
            obj.containing_post = containing_post
            obj.save()
            obj.keywords.set(saved_keywords)

            # Should be redundant (keywords.set() should call this anyways), but why not
            obj.save()

            return obj

    @staticmethod
    def add_sentences(sentences, embeddings, containing_posts, global_keywords=[]):
        """Inserts all provided sentences and their respective embeddings into the database. The same keywords are used
        for all sentences, the other parameters should be lists of equal length"""
        for index, sentence in enumerate(sentences):
            sentence_keywords = get_hotwords(text=sentence, amount=MAX_KEYWORDS_PER_EMBEDDED_SENTENCE)
            EmbeddedSentence.create_if_not_exists(embedding_vector=embeddings[index],
                                                  sentence=sentence,
                                                  keywords=sentence_keywords + global_keywords,
                                                  containing_post=containing_posts[index])

    def to_nlp_sentence(self):
        return Sentence(self.sentence,
                        text=self.containing_post.content,
                        link=self.containing_post.link,
                        title=self.containing_post.title,
                        author=self.containing_post.author,
                        containing_post=self.containing_post)


class UserQuery(models.Model):
    query_string = models.CharField(max_length=4069)
    request_time = models.DateTimeField(auto_now_add=True)
    keywords = models.ManyToManyField(Keyword)

    @classmethod
    def create(cls, query_string, keywords):
        # Create the keywords if they don't exist yet
        saved_keywords = []
        for keyword in keywords:
            saved_keywords.append(Keyword.create_if_not_exists(keyword))

        obj = cls()
        obj.query_string = query_string
        obj.save()
        obj.keywords.set(saved_keywords)
        # Should be redundant (keywords.set() should call this anyways), but why not
        obj.save()

        return obj

    # TODO: add a create() call in the API
