from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('results', views.results, name='results'),
    # path('dummer_test', views.dummer_test, name='dummer_test'),
]
