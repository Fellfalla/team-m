from django import forms

class TextInputField(forms.Form):
    text_input = forms.CharField(label='Type in your myth:',
        widget=forms.Textarea(
            attrs={'class':'form-control', 'placeholder':'e.g.: Cranberry Juice helps to prevent Kidney Stones.'}))
