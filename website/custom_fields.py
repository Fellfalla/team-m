from django.db import models
from array import array


class VectorField(models.BinaryField):
    """A field to store a vector in. Uses a BLOB"""

    def __init__(self, *args, **kwargs):
        # self.length = length        # TODO maybe add some checks to ensure this length or set the max_length accordingly
        super(VectorField, self).__init__(*args, **kwargs)

    # def clone(self):
    #     """Uses deconstruct() to clone a new copy of this Field. Preserves vector length."""
    #     name, path, args, kwargs = self.deconstruct()
    #     return self.__class__(length=self.length, *args, **kwargs)

    def deconstruct(self):
        """Returns all data required to reinstantiate this object, except for self.length"""
        name, path, args, kwargs = super().deconstruct()
        return name, path, args, kwargs

    def get_prep_value(self, value):
        binarized_value = array('d', value).tobytes()

        #print(f"binarized_value: {binarized_value}")

        return binarized_value

    def get_db_prep_value(self, value, connection, prepared=False):
        """value is the current value of the model’s attribute, and the method should return data in a format that has
        been prepared for use as a parameter in a query"""
        binarized_value = array('d', value).tobytes()

        #print(f"binarized_value: {binarized_value}")

        return binarized_value

        # finished_value = super().get_db_prep_value(binaried_value.tobytes(), connection, prepared)
        # if finished_value is not None:
        #     return connection.Database.Binary(binaried_value.tobytes())
        # return finished_value

    def from_db_value(self, value, expression, connection):
        """Converts a value as returned by the database to a Python object. It is the reverse of get_prep_value()"""
        return self.to_python(value)

    def to_python(self, value):
        """Converts a value as returned by the database to a Python object. It is the reverse of get_prep_value()"""
        binaried_value = value
        vector = array("d")

        vector.frombytes(binaried_value)
        return vector.tolist()

    def __str__(self):
        """Returns a string representation"""
        return f"Vector"

