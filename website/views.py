from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.template import loader
from django.views.decorators.csrf import csrf_exempt

from data_supply import get_data_by_query_sentence
from nlp.semantic_similarity import CONTROVERSIAL_LABEL, NEUTRAL_LABEL, ENTAILMENT_LABEL, CONTRADICTION_LABEL
from .forms import TextInputField

# Hacky way to relative import pyhton packages.
# The proper way would be to create one super package containing the entire app
import sys, os
sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), '..')))
from nlp import main as nlp_main
from nlp import preprocessing as nlp_preprocess


# TODO: adjust text names
color_dict = {
    ENTAILMENT_LABEL: "lightgreen",
    CONTRADICTION_LABEL: "indianred",
    CONTROVERSIAL_LABEL: "mediumpurple",
    NEUTRAL_LABEL: "lightgrey",
}

text_color_dict = {
    ENTAILMENT_LABEL: "dark",
    CONTRADICTION_LABEL: "light",
    CONTROVERSIAL_LABEL: "light",
    NEUTRAL_LABEL: "dark",
}

@csrf_exempt
def index(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = TextInputField(request.POST)
        # check whether it's valid:
        if form.is_valid():
            
            # process the data in form.cleaned_data as required

            # TODO call the NLP processor pipeline here
            print("##### Run NLP Pipeline #####")
            
            user_input = request.POST.get('text_input')
            #search_words = nlp_preprocess.get_hotwords(user_input, -1)

            similar_sentences, similarity_scores = get_data_by_query_sentence(user_input)

            #print(content_list)

            final_label, final_confidentiality,resources  = nlp_main.check_validity(user_input, similar_sentences, similarity_scores)
            # template = loader.get_template("results.html")
            # return HttpResponse(template.render())
            return render_results_page(final_label, round(final_confidentiality*100, 2), resources=resources)

    # if a GET (or any other method) we'll create a blank form
    else:
        form = TextInputField()

    template = loader.get_template("index.html")
    context = {
        "form": form
    }

    return HttpResponse(template.render(context))


def results(request):
    return render_results_page()


def render_results_resource_card(resource):
    result_resource_card_template = loader.get_template(
        "result_resource_card.html")

    label = resource["predicted_label"]
    context = {
        "resource_card_title": resource["title"],
        "resource_card_text": resource["text"],
        "resource_card_link": resource["link"],
        "resource_card_controversiality_color": color_dict[label],
        "resource_card_controversiality_text_color": text_color_dict[label],
        "resource_card_controversiality_text": label,
        "resource_card_controversiality_score": round(resource["predicted_score"] * 100),
    }

    return result_resource_card_template.render(context)


def render_results_page(controversiality_text="NEUTRAL", controversiality_score=69, resources=()):

    # resources = [
    #     {
    #         "title": "The Juice",
    #         "content": """Lorem ipsum, dolor sit amet consectetur adipisicing elit.
    #           Doloremque at quos dolorum nulla assumenda distinctio.""",
    #         "link": "#",
    #         "author": "Bengiz Chengiz",
    #     },
    #     {
    #         "title": "The Juice: Pre-Sequel",
    #         "content": """Lorem ipsum, dolor sit amet consectetur adipisicing elit.
    #           Doloremque at quos dolorum nulla assumenda distinctio.""",
    #         "link": "#",
    #         "author": "Bengiz Chengiz",
    #     },
    #     {
    #         "title": "Yalla Fahrt",
    #         "content": """Lorem ipsum, dolor sit amet consectetur adipisicing elit.
    #           Doloremque at quos dolorum nulla assumenda distinctio.""",
    #         "link": "#",
    #         "author": "Bengiz Chengiz",
    #     },
    #     {
    #         "title": "Cash Money",
    #         "content": """Lorem ipsum, dolor sit amet consectetur adipisicing elit.
    #           Doloremque at quos dolorum nulla assumenda distinctio.""",
    #         "link": "#",
    #         "author": "Bengiz Chengiz",
    #     },
    #     {
    #         "title": "Kidney Aua",
    #         "content": """Lorem ipsum, dolor sit amet consectetur adipisicing elit.
    #           Doloremque at quos dolorum nulla assumenda distinctio.""",
    #         "link": "#",
    #         "author": "Bengiz Chengiz",
    #     },
    #     {
    #         "title": "The Juice",
    #         "content": """Lorem ipsum, dolor sit amet consectetur adipisicing elit.
    #           Doloremque at quos dolorum nulla assumenda distinctio.""",
    #         "link": "#",
    #         "author": "Bengiz Chengiz",
    #     },
    # ]

    result_page_template = loader.get_template("results.html")

    resource_cards = "\n".join(map(render_results_resource_card, resources))

    result_page_template = loader.get_template("results.html")

    context = {
        "controversiality_text": controversiality_text,
        "controversiality_color": color_dict[controversiality_text],
        "controversiality_text_color": text_color_dict[controversiality_text],
        "controversiality_score": controversiality_score,
        "resource_cards": resource_cards,
    }
    return HttpResponse(result_page_template.render(context))
