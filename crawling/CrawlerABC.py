from abc import ABC, abstractmethod

from django.db.models import Count, Q

# from website.models import Scraper, ScrapedQuery, ScrapedPost, Keyword
import website.models


class CrawlerABC(ABC):
    def __init__(self):
        super().__init__()
        self.link = None

    def get_all_results(self, keywords_arr, num_results):
        if not self.has_previous_query(keywords=keywords_arr):
            return self.get_new_results(keywords=keywords_arr, num_results=num_results)

        cached_results = self.get_cached_results(keywords=keywords_arr)

        if cached_results.count() >= num_results:
            return cached_results
        else:
            new_required_count = num_results - cached_results.count()

        return cached_results + self.get_new_results(keywords=keywords_arr,
                                                     num_results=new_required_count)

    @abstractmethod
    def get_new_results(self, keywords, num_results):
        pass

    def get_database_entry(self):
        # TODO: have to do something about it XD -> temporary solution
        scraper = website.models.Scraper.create_if_not_exists(name="KidneyOrg",
                                                              link=self.link)
        return scraper
        # return website.models.Scraper.objects.get(link=self.link)

    def get_crawl_depth(self, keywords):
        previous_query = self.get_previous_query(keywords)
        if not previous_query:
            return 0
        return previous_query.crawl_depth

    def get_cached_results(self, keywords):
        previous_query = self.get_previous_query(keywords=keywords)
        if previous_query is None:
            return None

        return previous_query.results.all()

    def has_previous_query(self, keywords):
        db_keywords = website.models.Keyword.multiple_create_if_not_exist(keywords)

        return website.models.ScrapedQuery.objects\
            .annotate(keyword_match_count=Count('keywords', filter=Q(keywords__in=db_keywords)))\
            .filter(keyword_match_count__gte=len(keywords)).exists()

    def get_previous_query(self, keywords):
        db_keywords = website.models.Keyword.multiple_create_if_not_exist(keywords)

        existing_queries = website.models.ScrapedQuery.objects\
            .annotate(keyword_match_count=Count('keywords', filter=Q(keywords__in=db_keywords)))\
            .filter(keyword_match_count__gte=len(keywords))

        if existing_queries.exists():
            return existing_queries[0]
        else:
            return None

# class HiTest(Crawler):
#     def get_all_results(self, keywords_arr):
#         print(keywords_arr)
#         return 0

# def main():
#     x = HiTest()
#     print(x.get_all_results("HIIIIIIIIIIIIIIIIIIIII"))
#     print(x.get_database_entry())

# if __name__ == "__main__":
#     main()
