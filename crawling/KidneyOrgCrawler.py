# from selenium import webdriver
# from selenium.webdriver.chrome.options import Options
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC
# from selenium.webdriver.common.by import By
# from selenium.webdriver.common.keys import Keys
import requests
import re
from bs4 import BeautifulSoup

from crawling.CrawlerABC import CrawlerABC
# from website.models import ScrapedPost, Keyword, ScrapedQuery, Scraper
import website.models


class KidneyOrgCrawler(CrawlerABC):
    def __init__(self):
        super().__init__()
        self.RESULTS_PER_PAGE = 10
        self.MAX_RESULTS = 20
        self.link = "https://www.kidney.org/search-results?solr-keywords="

    def get_new_results(self, keywords, num_posts_to_crawl):
        """ This method crawls the kidney.org website
        Args:
            keywords - <list> list of keywords, used for search request
            num_posts_to_crawl - <integer> how many posts the crawler should crawl
        Returns:
            crawling_results - <list> returns crawling results 
        """

        starting_depth = super().get_crawl_depth(keywords)

        crawling_results = self.crawl_website(keywords, starting_depth, num_posts_to_crawl)

        return crawling_results

    def crawl_website(self, keywords_arr, explored_depth, num_posts_to_crawl):
        """Gets keywords and crawls based on starting_depth and end_depth"""
        MAX_RESULTS = num_posts_to_crawl
        starting_depth = explored_depth + 1
        starting_post = starting_depth * 10 + 1

        saved_keywords = []
        for keyword in keywords_arr:
            saved_keyword = website.models.Keyword.create_if_not_exists(keyword)
            saved_keywords.append(saved_keyword)

        # parse kidneyorg html starting at starting_depth
        # TODO: don't re-request page 0
        page_1 = self.get_kidney_org_html(keywords_arr, 1)
        soup = BeautifulSoup(page_1, "html.parser")

        # ugly workaround
        try:
            results_count = int(soup.find("div", "view-header").find("em").string.split(" ")[-1])
        except Exception as e:
            raise Exception("PLEASE ENTER A VALID SEARCH QUERY.")

        # crawling starts
        results = []
        page_nr = explored_depth

        for post_num in range(starting_post, min(MAX_RESULTS + starting_post, results_count), 10):
            page_nr = page_nr + 1
            results += self.processRequest(keywords_arr=saved_keywords, page_nr=page_nr)

        if len(results) == 0:
            raise Exception("NO MATCHING SENTENCES FOUND")

        if explored_depth > 1:
            prev_query = super().get_previous_query(keywords=saved_keywords)
            print(f"Updating query {prev_query} to depth {page_nr} with keywords {saved_keywords}")
            prev_query.update(page_nr, results)
        else:
            website.models.ScrapedQuery.create_if_not_exists(super().get_database_entry(),
                                                             keywords=saved_keywords,
                                                             results=results,
                                                             crawl_depth=page_nr)

        return results

    def processRequest(self, keywords_arr, page_nr):
        """Gets HTML, results, calls the print function"""
        print(f"Processing page {page_nr} with keywords {keywords_arr}.")
        results_html = self.get_kidney_org_html(keywords_arr=keywords_arr, page_nr=page_nr)
        result_objs = self.parse_kidney_org_html(results_html)

        scraped_objs = []
        for result_obj in result_objs:
            scraped_obj = self.save_to_database(result_obj, keywords_arr)
            scraped_objs.append(scraped_obj)

        return scraped_objs

    def save_to_database(self, result_obj, keyword_arr):
        """Outputs parsed crawling results"""

        title, link, content, author = self._parse_kidney_org_result(result_obj)

        scraped_post = website.models.ScrapedPost.create_if_not_exists(title=title,
                                                                       link=link,
                                                                       content=content,
                                                                       author=author,
                                                                       publication_date=None,
                                                                       keywords=keyword_arr)

        return scraped_post

    def get_kidney_org_html(self, keywords_arr, page_nr=None):
        """Appends page number and keywords"""

        keyword_string = "+".join([str(keyword) for keyword in keywords_arr])

        if page_nr is None:
            return requests.get("{}{}".format(self.link, keyword_string)).text
        else:
            return requests.get("{}{}&page={}".format(self.link, keyword_string, page_nr)).text

    def parse_kidney_org_html(self, kidney_org_html):
        """Gets HTML version of kidney.org site"""

        soup = BeautifulSoup(kidney_org_html, "html.parser")

        results = soup.find_all("div", "search-result")
        return results

    def _parse_kidney_org_result(self, result_obj):
        """Parses single searching results into title, link, text, author"""

        # TODO: also extract the publication date

        title = result_obj.find("h2").find("a").string
        link = result_obj.find("h2").find("a").attrs["href"]
        all_spans = result_obj.find_all("span")
        author = None
        content = None

        if len(all_spans) > 0:
            content = all_spans[0]
            if not content is None:
                content = self._clean_content_text(content)
                content = self._extract_content(content, link)

            if len(all_spans) > 1:
                author = self._clean_content_text(all_spans[2])

        return title, link, content, author

    def _clean_content_text(self, content):
        """Removes unnecessary HTML tags, break lines, urls"""

        cleanr = re.compile('<.*?>')
        cleantext = re.sub(cleanr, '', str(content))
        cleantext = cleantext.replace('\n', ' ')
        cleantext = re.sub(r"http\S+", "", cleantext)

        return cleantext

    def _extract_content(self, text, link):
        """Extracts whole text"""

        last_3_chars = text[-3:]
        if not last_3_chars == "...":
            return text

        new_html = requests.get(link).text
        soup = BeautifulSoup(new_html, "html.parser")

        cleaned_text = soup.findAll("div", id="content")
        cleaned_text = self._clean_content_text(cleaned_text)

        return cleaned_text

        # print("\nResult {}".format(result_count))
        # print("Title: {}\nLink: {}\nContent: {}\nSource: {}\n".format(title, link, content, author))
        # print("\n------------------------------\n")

        # result_count += 1
        # print("BOBs and vagene")

        # def save_all_results(self, keywords_arr):
    #     """This function is called from the outside. Gets keywords and saves contents"""

    #     page_1 = get_kidney_org_html(keywords_arr)
    #     soup = BeautifulSoup(page_1, "html.parser")
    #     results_count = int(soup.find("div", "view-header").find("em").string.split(" ")[-1])

    #     # TODO don't re-request page 0
    #     results = []

    #     for result_index in range(1, min(MAX_RESULTS, results_count), 10):
    #         page_nr = int(result_index / 10)
    #         results += self.save_kidney_org_results(keywords_arr=keywords_arr, page_nr=page_nr)

    #     return results

    # def fetch_kidney_org_results(self, keywords_arr, page_nr):
    #     """Gets HTML, results, calls the print function"""

    #     results_html = get_kidney_org_html(keywords_arr=keywords_arr, page_nr=page_nr)
    #     result_objs = parse_kidney_org_html(results_html)

    #     parsed_objs = []

    #     for result_obj in result_objs:
    #         parsed_objs += [fetch_kidney_org_result(result_obj, keyword_arr=keywords_arr)]

    #     return parsed_objs

    # def fetch_kidney_org_result(self, result_obj, keyword_arr):
    #     title, link, content, author = _parse_kidney_org_result(result_obj)

    #     scraped_post = ScrapedPost.create_if_not_exists(title=title,
    #                                                     link=link,
    #                                                     content=content,
    #                                                     author=author,
    #                                                     publication_date=None,
    #                                                     keywords=keyword_arr)

    #     return title, link, content, author

    # def get_content_from_keywords(self, keyword_arr):
    #     """Returns a list of tuples of type (title + content, url)"""
    #     saved_keywords = []

    #     for keyword in keyword_arr:
    #         saved_keyword = Keyword.create_if_not_exists(keyword)
    #         saved_keywords.append(saved_keyword)

    #     results = crawl_website(keywords_arr=saved_keywords)

    #     reformatted_results = []

    #     for result in results:
    #         title = ""
    #         content = ""
    #         link = ""

    #         if result[0] is not None:
    #             title = result[0]

    #         if result[1] is not None:
    #             link = result[1]

    #         if result[2] is not None:
    #             content = result[2]

    #         reformatted_results.append((title + " " + content, link))

    #     return reformatted_results

    # def save_all_results_from_keywords_string(self, keyword_string):
    #     """Returns a list of tuples of type (title + content, url)"""

    #     keywords = keyword_string.split(" ")

    #     saved_keywords = []

    #     for keyword in keywords:
    #         saved_keyword = Keyword.create_if_not_exists(keyword)
    #         saved_keywords.append(saved_keyword)

    #     results = crawl_website(keywords_arr=saved_keywords)

    #     return results
