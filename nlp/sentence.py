
class Sentence(object):
    def __init__(self, content, **meta_data):
        """
        Args:
            content - String containing the content of the sentence
            meta_data - additional meta data
        """

        self.content = content
        self.custom_attributes = []
        self.add_custom_meta_data(**meta_data)

    def add_custom_meta_data(self, **meta_data):
        # Keep track of custom meta data
        self.custom_attributes.extend(meta_data.keys())

        # Add to the class
        for key, value in meta_data.items():
            setattr(self, key, value)

    def to_dict(self):
        d = {key: getattr(self, key) for key in self.custom_attributes}
        d['content'] = self.content
        return d

    def __str__(self):
        return self.content

    def __repr__(self):

        meta_data = {}

        for attr in self.custom_attributes:
            val = getattr(self, attr)
            meta_data[attr] = val

        return "%s %s"%(self.content, meta_data)

if __name__ == "__main__":
    a = Sentence("this is a sentence", link="some_link")

    print(a)
    print(repr(a))

    print(a.to_dict())