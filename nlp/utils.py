import numpy as np


def cosine_similarity(vec1, vec2):
    """
    Source: https://stackoverflow.com/a/53456596
    """
    return np.dot(vec1, vec2) / (np.linalg.norm(vec1) * np.linalg.norm(vec2))


def softmax(x):
    """
    Compute softmax values for each sets of scores in x.
    Source: https://stackoverflow.com/a/38250088
    """
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum(axis=0) # only difference