from transformers import AutoTokenizer, AutoModelForSequenceClassification
import torch
import numpy as np

##### Global definitions of the configuration 
# Source Config: https://huggingface.co/roberta-large-mnli/blob/main/config.json
CONTROVERSIAL_LABEL = "CONTROVERSIAL"
NEUTRAL_LABEL = "NEUTRAL"
ENTAILMENT_LABEL = "CONFIRMATION"
CONTRADICTION_LABEL = "CONTRADICTION"


id2label = {
    0: CONTRADICTION_LABEL,
    1: NEUTRAL_LABEL,
    2: ENTAILMENT_LABEL,
    3: CONTROVERSIAL_LABEL,
}

label2id = {v:k for k, v in id2label.items() }


##### Load the NLP Models and Tokenizers. 
##### This might take a while
tokenizer = AutoTokenizer.from_pretrained("textattack/roberta-base-MNLI")
model = AutoModelForSequenceClassification.from_pretrained("textattack/roberta-base-MNLI")


def get_semantic_similarity(sentence1, sentence2):
    """
    Compares 2 sentences for their semantic similarity.

    Args:
        sentence1 - String representing the first sentence
        sentence2 - String representing the second sentence

    Returns:
        label - String representation of the predicted outcome
        confidence - float representing the confidence in the current outcome
        prediction - the raw prediciton with for all three values (CONTRADICTION, NEUTRAL, ENTAILMENT)

    """
    # semantic similarity using the language model
    encoding = tokenizer.encode_plus(sentence1, sentence2, return_tensors="pt")
    prediction_logits = model(**encoding)[0]
    
    # postprocess the prediction
    prediction = torch.softmax(prediction_logits, dim=1).tolist()[0]
    result = np.argmax(prediction)
    confidence = prediction[result]
    label = id2label[result] 

    return label, confidence, prediction

if __name__ == '__main__':

    sentence_0 = "George is gay"
    sentence_1 = "Every gay guy is named george"
    sentence_2 = "George has a girlfriend"

    paraphrase_result, paraphrase_results_score = get_semantic_similarity(sentence_0, sentence_1)
    not_paraphrase_result, not_paraphrase_results_score = get_semantic_similarity(sentence_0, sentence_2)

    print("Should be entailed:")
    print("%s %.3f%%"%(paraphrase_result, paraphrase_results_score*100))
    print()
    
    print("Should be a contradiction")
    print("%s %.3f%%"%(not_paraphrase_result, not_paraphrase_results_score*100))
    print()
