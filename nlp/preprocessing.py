import spacy
import nltk
from string import punctuation

# Local Imports
import sys, os
sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), '..')))
from nlp.sentence import Sentence

nlp = spacy.load("en_core_web_sm")

def split_sentences(data):
    # nlp.add_pipe(nlp.create_pipe('sentencizer')) # updated
    doc = nlp(data)
    sentences = [sent.string.strip() for sent in doc.sents]
    return sentences

def get_hotwords(text, amount):
    result = []
    pos_tag = ['PROPN', 'NOUN']  # ['PROPN', 'ADJ', 'NOUN', "VERB"]
    doc = nlp(text.lower()) 
    for token in doc:
       
        if(token.text in nlp.Defaults.stop_words or token.text in punctuation):
            continue
        
        if(token.pos_ in pos_tag):
            result.append(token.lemma_)

    fdist = nltk.FreqDist(result)  
    temp = [str(elem) for elem in fdist]

    hotlist = []
    if amount == -1:
        amount = len(temp)

    for i in range(min(amount, len(temp))):
        hotlist.append(temp[i])       

    return hotlist   


def extend_data(data):
    """
    input: list of tuples (title, link, text, author)
    output: extended list of touples (link, sentences)
    """
    new_data=[]

    for datum in data:
        if datum.title != None:
            content = datum.title
            if datum.content != None:
                content = content+". "+datum.content
            content = str(content)
            sentences = split_sentences(content)
            for sent in sentences: 
                new_data.append(Sentence(sent,
                                         text=datum.content,
                                         link=datum.link,
                                         title=datum.title,
                                         author=datum.author,
                                         containing_post=datum))
    
    return new_data
        

def preprocess(data):
    """
    input: touples of (data, link)
    """

    data = extend_data(data)

    
    #data = split_sentences(data)
    #TODO remove stopwords, later print out raw sentence based on index
    return data


if __name__ == "__main__":

    sentences = [
        ('This framework generates embeddings for each input sentence',"link"),
        ('The quick brown fox jumps over the lazy dog.', "link"),
        ('cranberry juice is very unhealthy.', "link"),
        ('You should drink cranberry juice to get rid of kidney stones', "link"),
        ('Sentences are passed as a list of string.',  "link"),
        ('You should not drink cranberry juice to get rid of kidney stones', "link"),
    ]

    out = preprocess(sentences)

    for elem in out: 
        print(elem.content)