import nltk
from nltk.sentiment.vader import SentimentIntensityAnalyzer

nltk.download('vader_lexicon')
analyzer = SentimentIntensityAnalyzer()

def predict(sentence):
    scores = analyzer.polarity_scores(sentence)
    # print(scores)
    return scores

def predict_list(sentence_list):
    sentiments = []
    for sent in sentence_list:
        sent_prediction = predict(sent)
        sentiments.append(sent_prediction)
    
    return sentiments

predict("sentence")