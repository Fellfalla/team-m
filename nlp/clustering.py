import numpy as np

from sentence_transformers import SentenceTransformer
sentence_transformer_model = SentenceTransformer('paraphrase-distilroberta-base-v1')

# Local Imports
import sys, os
sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), '..')))
from nlp import utils
from nlp.sentence import Sentence


def get_sentence_embedding(sentences):
    """
    Args:
        sentences - List of sentences
    """
    # Create an embedding for each sentence
    return sentence_transformer_model.encode(sentences)

def get_similar_sentences(input_text, data, minimum_similarity, n_max=None, verbose=True):
    """
    Args:
        input_text - String that we want to have similar sentences to
        sentences - List of sentences that make up our knowledge base
        minimum_similarity - float required score for 2 sentences to be considered talking about similar stuff
        n_max - max number of sentences

    Returns:
        similar_sentences - List of n most similar (class) Sentence. Sorted by similarity score
        similarity_scores_sorted - list of scores
    """

    data = np.array(data)
    sentences = [elem.content for elem in data]
    sentences = np.array(sentences)

    sentence_embeddings = get_sentence_embedding(sentences)
    input_embedding = get_sentence_embedding(input_text)

    similarity_scores = []
    for sentence, embedding in zip(sentences, sentence_embeddings):

        # Look up similarity
        similarity = utils.cosine_similarity(input_embedding, embedding)
        similarity_scores.append(similarity)

        if verbose:
            # print("Embedding:", embedding, end="\t")
            # print("Length of embedding vec: %i"%len(embedding))

            print("Sentence:", sentence, end="\n")
            print("Similarity: %f"%(similarity))
            print("")

    similarity_scores = np.array(similarity_scores)

    # Sort the result
    indices = np.argsort(similarity_scores)[::-1]
    similar_sentences_sorted = np.take_along_axis(data, indices, axis=0)
    similarity_scores_sorted = np.take_along_axis(similarity_scores, indices, axis=0)

    embedding_length = sentence_embeddings.shape[1]
    sentence_embeddings_sorted = np.take_along_axis(sentence_embeddings,
                                                    # Add a dimension to the indices and fill it
                                                    indices.reshape((indices.shape[0], 1))
                                                    .repeat(embedding_length, axis=1), axis=0)

    # Determine the number of sentences to return
    n_similar = np.count_nonzero(similarity_scores > minimum_similarity)
    if n_max is None:
        n = n_similar
    else:
        n = min(n_similar, n_max)

    return similar_sentences_sorted[:n], similarity_scores_sorted[:n], sentence_embeddings_sorted[:n]

# TODO maybe implement this in a cleaner, less copy-paste way
def get_similar_cached_sentences(input_text, cached_data, cached_sentences, minimum_similarity, n_max=None,
                                 verbose=True):
    """
    Args:
        input_text - String that we want to have similar sentences to
        cached_data - List of Sentence objects containing comparable data
        cached_sentences - List of EmbeddedSentence objects containing embedding vectors
        sentences - List of sentences that make up our knowledge base
        minimum_similarity - float required score for 2 sentences to be considered talking about similar stuff
        n_max - max number of sentences

    Returns:
        similar_sentences - List of n most similar (class) Sentence. Sorted by similarity score
        similarity_scores_sorted - list of scores
    """
    data = np.array(cached_data)
    sentences = [elem.content for elem in data]
    sentences = np.array(sentences)

    sentence_embeddings = [cached_sentence.embedding_vector for cached_sentence in cached_sentences]
    input_embedding = get_sentence_embedding(input_text)

    similarity_scores = []
    for sentence, embedding in zip(sentences, sentence_embeddings):

        # Look up similarity
        similarity = utils.cosine_similarity(input_embedding, embedding)
        similarity_scores.append(similarity)

        if verbose:
            # print("Embedding:", embedding, end="\t")
            # print("Length of embedding vec: %i"%len(embedding))

            print("Sentence:", sentence, end="\n")
            print("Similarity: %f"%(similarity))
            print("")

    similarity_scores = np.array(similarity_scores)

    # Sort the result
    indices = np.argsort(similarity_scores)[::-1]
    similar_sentences_sorted = np.take_along_axis(data, indices, axis=0)
    similarity_scores_sorted = np.take_along_axis(similarity_scores, indices, axis=0)

    # Determine the number of sentences to return
    n_similar = np.count_nonzero(similarity_scores > minimum_similarity)
    if n_max is None:
        n = n_similar
    else:
        n = min(n_similar, n_max)

    return similar_sentences_sorted[:n], similarity_scores_sorted[:n]


if __name__ == "__main__":

    # This is going to be the text coming from the user
    input_text = "Cranberry juice is healthy"

    # These are the sentences from our knowledge base
    # sentences = crawler.get_sentences()
    # sentences = ['This framework generates embeddings for each input sentence',
    #     'Sentences are passed as a list of string.', 
    #     'The quick brown fox jumps over the lazy dog.',
    #     'cranberry juice is very unhealthy.',
    #     'You should drink cranberry juice to get rid of kidney stones',
    #     'You should not drink cranberry juice to get rid of kidney stones',
    #     ]

    sentences = [
        Sentence('This framework generates embeddings for each input sentence',link="link"),
        Sentence('The quick brown fox jumps over the lazy dog.', link="link"),
        Sentence('cranberry juice is very unhealthy.', link="link"),
        Sentence('You should drink cranberry juice to get rid of kidney stones', link="link"),
        Sentence('Sentences are passed as a list of string.', link= "link"),
        Sentence('You should not drink cranberry juice to get rid of kidney stones', link="link"),
    ]
    
    result = get_similar_sentences(input_text, sentences, minimum_similarity=0.4, n_max=None, verbose=True)
    for i in range(len(result)):
        print("%i. %s"%(i+1, result[i]))
