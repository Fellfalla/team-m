import numpy as np

# Local Imports
# import crawler
from scipy.interpolate import interp1d

import nlp.preprocessing
import nlp.clustering
import nlp.semantic_similarity
import nlp.utils

# Local Imports
import sys, os

from data_supply import MIN_SENTENCE_SIMILARITY
from website.models import ScrapedPost, EmbeddedSentence

sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), '..')))
from nlp.sentence import Sentence


def check_validity(input_sentence, unsorted_sentences, unsorted_similarities):
    """
        input: list of maps {}
        output: 
            label ('CONFIRMATION', 'NEUTRAL', 'CONTRADICTION'])
            confidentiality (value between 0 and 1)
            ressources - list of Sentence objects with (dynamic) attributes: text,link,title,author,content,predicted_label,predicted_score (later: save embedding here too for DB)
    """

    # get content from crawler
    # data = crawler.get_text()
    print("##### User Input #####")
    print(input_sentence)
    print()

    sorted_indices = list(reversed(np.argsort(unsorted_similarities)))
    similar_sentences = unsorted_sentences[sorted_indices]
    similarity_scores = unsorted_similarities[sorted_indices]


    #### for testing:###
    # data = ['This framework generates embeddings for each input sentence',
    # 'Sentences are passed as a list of string.', 
    # 'The quick brown fox jumps over the lazy dog.',
    # 'cranberry juice is very unhealthy.',
    # 'You should drink cranberry juice to get rid of kidney stones',
    # 'You should not drink cranberry juice to get rid of kidney stones',
    # ]
    # data = "\n".join(data)



    # print("##### Clustered #####")
    # for i in range(len(similar_sentences)):
    #     print("%.2f: %s"%(similarity_scores[i], similar_sentences[i]), end="\n")
    # print()


    # Semantics of sentences
    print("##### Semantics #####")
    predictions = []
    similar_sentences_with_semantics = []
    for sent in similar_sentences:
        label, confidence, prediction = nlp.semantic_similarity.get_semantic_similarity(input_sentence, sent.content)
        predictions.append(prediction)
        index= np.argmax(prediction)
        predicted_score=prediction[index]
        predicted_label=nlp.semantic_similarity.id2label[index]
        sent.add_custom_meta_data(predicted_score=predicted_score, predicted_label=predicted_label)
        similar_sentences_with_semantics.append(sent)
    predictions= np.array(predictions)

    for i in range(predictions.shape[0]):
        print("%i. %s"%(i+1, similar_sentences[i]), end="\t")

        print("---", end="")
        for id in range(len(predictions[i])):
            label = nlp.semantic_similarity.id2label[id]
            print("- %.2f%% %s"%(predictions[i][id]*100, label), end=" ")
        print()

    print()

    # Finalizing claculations
    if len(predictions) == 0:
        # Catch case of no results
        final_score = np.array([0, 1, 0])
    else:
        # add more weight to similar sentences, because we expect themm to be more relevant
        mapper = interp1d([MIN_SENTENCE_SIMILARITY, max(similarity_scores)],[MIN_SENTENCE_SIMILARITY,1])
        strength_of_opinion = (1-predictions[:,1]) # neutrality is the opposite of a strong opinion
        score_weights = mapper(similarity_scores) * strength_of_opinion # similarity * stength of opinion
        final_score = np.average(predictions, axis=0, weights=score_weights)
        assert final_score.shape[0] == 3


    # Get the controversiality
    controversiality = min(final_score[0], final_score[-1])
    # final_score[0] = final_score[0] - controversiality
    # final_score[-1] = final_score[-1] - controversiality
    final_score = np.array([*final_score, controversiality*2]) # Append the controversiality label to the score array
    print()

    print("##### Finalizing #####")

    # get the final result
    # final_score = nlp.utils.softmax(final_score)
    final_type = np.argmax(final_score)
    final_label = nlp.semantic_similarity.id2label[final_type]
    final_confidentiality = final_score[final_type]

    # print stuff for the developer
    print("%s"%(input_sentence), end=" ---")
    
    for key, val in nlp.semantic_similarity.id2label.items():
        print("- %.2f%% %s"%(final_score[key]*100, val), end=" ")

    print()


    resources = [elem.to_dict() for elem in similar_sentences_with_semantics]

    return final_label, final_confidentiality, resources


if __name__ == "__main__":

    ### for testing:###
    # title, link, text, author
    data = [
        ('1', 'dummy', 'This framework generates embeddings for each input sentence', 'team-m'),
        ('1', 'dummy', 'Sentences are passed as a list of string.',  'team-m'),
        ('1', 'dummy', 'The quick brown fox jumps over the lazy dog.', 'team-m'),
        ('1', 'dummy', 'cranberry juice is very unhealthy.', 'team-m'),
        ('1', 'dummy', 'You should drink cranberry juice to get rid of kidney stones', 'team-m'),
        ('1', 'dummy', 'You should not drink cranberry juice to get rid of kidney stones', 'team-m'),
    ]
    # data = "\n".join(data)

    check_validity("Cranberry juice is healthy", data)