


# Team M 

An API which takes fact checks a controversial statement about kidney stone.

## Install requirements
```
pip install -r requirements.txt
```

## Initialize database
```
python manage.py makemigrations website
python manage.py migrate
```

## Run Server locally
```
python manage.py runserver 0.0.0.0:8080
```
## Access

```
You can access a testing frontend running on the server itself at 127.0.0.1:8080
You can access the graphql API at at 127.0.0.1:8080/graphql
```




Interesting Links:

https://miro.com/app/board/o9J_kgJQLYo=/

https://gitlab.com/Fellfalla/team-m

https://www.youtube.com/watch?v=B5XZBXaco1Y

https://spacy.io/usage/spacy-101

https://www.fakerfact.org/

https://github.com/SimonHegelich/ADL-NLP

