
def get_text():
    return " Skip to main content \
Español \
\
Donate \
\
Menu \
Search \
drinking cranberry juice is dangerous \
Displaying 1 - 10 of 1310 \
What are the effects of drinking cranberry juice? \
Cranberries are full of antioxidants, as well a source of Vitamin C and fiber. One cup of whole cranberries is only 46 calories! Click on the link below to read more about the cranberry kidney connection: http://www.kidney.org/news/keephealthy/newsletter/fallwinter2013/KH_Cranberry-Kidney-Connection.cfm Speak to your health care... \
Q: Is cranberry juice a healthy drink for someone with increased creatinine levels? \
A: Cranberry juice is very low in potassium and has been shown in randomized trials to prevent urinary tract infections in ladies with recurrent infections. It can be safely used in patients with very low kidney function, even in Stage 4 chronic kidney disease with elevated creatinine levels. \
Blog: Ask the Doctor Blog \
I have just been diagnosed with stage 3 kidney disease. My creatinine number is 1.20 and my eGFr levels are 47 and 54. I have an appointment with a nephrologist on Nov. 12th (I couldn’t get one sooner). Am I in any danger from waiting this long for my appointment? Also, is it beneficial for me to drink 100% pure cranberry juice? \
Cranberry juice has been shown to prevent bladder infections in ladies.  I am unable to make a diagnosis based on the information that you present.  If your primary care physician made the referral to a nephrologist, this should be acceptable.  The estimated glomerular filtration rate (eGFR) that you provide are consistent with early Stage 3 chronic kidney disease (CKD). \
Blog: Ask the Doctor Blog \
To Juice or Not to Juice? \
By Linda Ulerich, RD \
\
A diet rich in fruits and vegetables reduces the risk for heart disease and stroke, protects against certain types of cancer, and provides fiber which reduces the risk of obesity, type2 diabetes, and diverticulosis. Juicing is being touted as a way to lose weight and stay healthy, and while it may seem like a recent trend, juicing has been around for ages. As with anything related to diet, there are positives and... \
\
I have heard that drinking cranberry or lemon juice before/during/after a kidney stone can help get rid of it as well as prevent getting another one. Is this true? If so, how does that work? \
Drinking pure lemon juice has been shown to decrease the risk of forming calcium stones. It is thought to do so by increasing the amount of citrate in the urine to prevent calcium crystals from forming into larger stones. I have not heard about anything related to cranberry juice and stone disease. Cranberry juice has been shown to decrease the risk of bladder infections in women with recurrent urinary tract infections (UTI's). \
Blog: Ask the Doctor Blog \
Other than water and cranberry juice, what other liquids can you drink to help protect your kidneys? Should all soda be avoided including the non-caffeine kind? \
Water and cranberry juice are safe and cranberry juice will protect you against getting bladder infections. Soda, especially dark sodas, are high in phosphorus and may not be good for you with chronic kidney disease (CKD). Sodas of all kinds have been associated with obesity, diabetes and hypertension, which are not good for kidneys. The soda itself may not be bad for kidneys but the diseases that are associated with soda drinking are also... \
Blog: Ask the Doctor Blog \
Went for a checkup and was told that I have protein in my urine. I was told for a women it's rare and to go for follow up but I do not have insurance. Is this something I should really be worried about? I've been drinking a lot of cranberry juice. \
Protein in the urine can be a sign of chronic kidney disease (CKD). This will depend on the amount of protein that you have in the urine. You should have the protein measured more exactly. The more protein, the more likely this is CKD. \
Blog: Ask the Doctor Blog \
My brother used to drink a lot and use drugs. Since he's been with me, he's eating right and taking vitamins. After eating though, his kidney aches. Cranberry juice freshly juiced helps a lot. Any special vitamins to take?\
I am unable to make a diagnosis based on the information presented. Aching in the kidneys is not a reliable sign of kidney disease. I suggest he consult with a physician for a specific diagnosis and treatment plan. Vitamins are unlikely to help. \
Blog: Ask the Doctor Blog \
Survey Shows Education and Communication Gaps in Lupus Nephritis Care \
1 in 2 Lupus Nephritis Patients Don’t Know They Have Lupus \
\
New York, NY– December 7, 2020—Significant gaps in education and communication among patients and physicians pose barriers to lupus nephritis (LN) care, according to results of two companion surveys conducted by the National Kidney Foundation (NKF) and the Lupus Research Alliance (LRA).  \
\
Half of all people with LN do not realize that, by definition, they... \
\
NKF and RenalytixAI Partner to Improve Early Stage Kidney Disease \
Program focused on improving disease management and outcomes beginning at the primary care level "



def get_sentences():
    """
    Returns a list of sentences
    """


    return get_text.split('\n')
