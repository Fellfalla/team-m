from data_supply import get_data_by_query_sentence
from nlp import preprocessing as nlp_preprocess
from nlp import main as nlp_main
import graphene

# Hacky way to relative import pyhton packages.
# The proper way would be to create one super package containing the entire app
import sys
import os
sys.path.append(os.path.normpath(
    os.path.join(os.path.dirname(__file__), '..')))


class AnalysisResult(graphene.ObjectType):
    controversiality = graphene.String()
    confidence_score = graphene.Float()


class ResourceItem(graphene.ObjectType):
    text = graphene.String()
    link = graphene.String()
    title = graphene.String()
    author = graphene.String()
    content = graphene.String()
    predicted_label = graphene.String()
    predicted_score = graphene.Float()


class AnalysisResponse(graphene.ObjectType):
    result = graphene.Field(AnalysisResult)
    resources = graphene.List(ResourceItem)


class Query(graphene.ObjectType):
    analyze_sentence_controversiality = graphene.Field(
        AnalysisResponse, sentence=graphene.String(required=True))

    def resolve_analyze_sentence_controversiality(root, info, sentence):

        print("##### Run NLP Pipeline #####")

        user_input = sentence
        search_words = nlp_preprocess.get_hotwords(user_input, -1)

        similar_sentences, similarity_scores = get_data_by_query_sentence(user_input)

        # print(content_list)

        final_label, final_confidentiality, resources = nlp_main.check_validity(user_input, similar_sentences,
                                                                                similarity_scores)

        return {
            "result": {
                "controversiality": final_label,
                "confidence_score": final_confidentiality,
            },
            "resources": resources
            # [
            #     {
            #         "analysis": {
            #             "controversiality": "CONTRADICTION",
            #             "confidence_score": 69,
            #         },
            #         "url": "www.asdf1.de",
            #         "title": "myTitle1",
            #         "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
            #     },
            #     {
            #         "analysis": {
            #             "controversiality": "NEUTRAL",
            #             "confidence_score": 69,
            #         },
            #         "url": "www.asdf2.de",
            #         "title": "myTitle2",
            #         "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
            #     },
            #     {
            #         "analysis": {
            #             "controversiality": "ENTAILMENT",
            #             "confidence_score": 69,
            #         },
            #         "url": "www.asdf3.de",
            #         "title": "myTitle3",
            #         "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
            #     },
            #     {
            #         "analysis": {
            #             "controversiality": "CONTRADICTION",
            #             "confidence_score": 69,
            #         },
            #         "url": "www.asdf4.de",
            #         "title": "myTitle4",
            #         "description": "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
            #     },
            # ],
        }


schema = graphene.Schema(query=Query)
