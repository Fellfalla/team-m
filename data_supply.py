from django.db.models import Count, Q
import numpy as np
from scipy.spatial import distance

from crawling.KidneyOrgCrawler import KidneyOrgCrawler
from nlp.clustering import get_similar_cached_sentences, get_similar_sentences, get_sentence_embedding
from nlp.preprocessing import get_hotwords, preprocess
from website.models import EmbeddedSentence, Keyword, UserQuery

MIN_KEYWORD_PERCENTAGE_MATCHING = 1
MIN_KEYWORDS_MATCHING = 3
MIN_SENTENCES_REQUIRED = 10

MIN_SENTENCE_SIMILARITY = 0.5

MAX_CRAWL_ROUNDS = 5
POSTS_PER_ROUND = 20

CURSED_KEYWORDS = ("kidney", "kidneys")  #, "stone", "stones")

crawlers = [KidneyOrgCrawler()]


def get_data_by_query_sentence(input_string):
    """Decides wheter to crawl for new content or cached information is sufficient, and accordingly returns new and/or
    cached data"""

    # Yeet this cosine-distance based approach for now, as it does not really work as intended: It will find mostly
    # agreeing sentences if at all, which kind of counteracts the original purpose

    similar_sentences = np.array([])
    similarity_scores = np.array([])

    if EmbeddedSentence.objects.all().exists():

        input_vector = get_sentence_embedding(input_string)

        all_sentences = list(EmbeddedSentence.objects.all())
        existing_embeddings = np.array([sentence.embedding_vector for sentence in all_sentences])

        distances = distance.cdist([input_vector], existing_embeddings, "cosine")[0]
        similarities = 1 - distances
        relevant_indices = (similarities > MIN_SENTENCE_SIMILARITY)

        cos_similar_sentences = np.array([sentence.to_nlp_sentence()
                                          for sentence in np.array(all_sentences)[relevant_indices]])
        cos_similarity_scores = np.array(similarities[relevant_indices])
        similar_sentences = np.append(similar_sentences, cos_similar_sentences)
        similarity_scores = np.append(similarity_scores, cos_similarity_scores)

        print(f"Found {len(similarity_scores)} semantically similar sentences:")
        for index, sentence in enumerate(cos_similar_sentences):
            print(f"\n{sentence.content}, similarity: {cos_similarity_scores[index]}")

    # Find sentences with similar keywords

    input_keywords = get_hotwords(input_string, -1)
    input_keywords = [input_keyword for input_keyword in input_keywords if input_keyword not in CURSED_KEYWORDS]
    UserQuery.create(query_string=input_string, keywords=input_keywords)

    keyword_objs = Keyword.multiple_create_if_not_exist(input_keywords)

    min_matching_keywords = max(int(np.ceil(len(keyword_objs) * MIN_KEYWORD_PERCENTAGE_MATCHING)),
                                min(MIN_KEYWORDS_MATCHING, len(keyword_objs)))

    cached_sentences = (EmbeddedSentence.objects.annotate(
        keyword_match_count=Count('keywords', filter=Q(keywords__in=keyword_objs))).filter(
        keyword_match_count__gte=min_matching_keywords))

    if cached_sentences.exists():
        c_data = [sentence.to_nlp_sentence() for sentence in cached_sentences]

        # Get top n of similar texts
        k_similar_sentences, k_similarity_scores = \
            get_similar_cached_sentences(input_string,
                                         cached_data=c_data,
                                         cached_sentences=cached_sentences,
                                         minimum_similarity=MIN_SENTENCE_SIMILARITY,
                                         n_max=None,
                                         verbose=False)
        similar_sentences = np.append(similar_sentences, k_similar_sentences)
        similarity_scores = np.append(similarity_scores, k_similarity_scores)

        print(f"Found {len(k_similar_sentences)} sentences with similar keywords:")
        for index, sentence in enumerate(k_similar_sentences):
            print(f"\n{sentence.content}, similarity: {k_similarity_scores[index]}")

    rounds_crawled = 0

    if len(similar_sentences) > 1:
        content_list = [similar_sentence.content for similar_sentence in similar_sentences]
        unique_indices = np.array([content_list.index(single_content) for single_content in set(content_list)])
        if len(unique_indices) > 0:
            similar_sentences = similar_sentences[unique_indices]
            similarity_scores = similarity_scores[unique_indices]

    while len(similar_sentences) <= MIN_SENTENCES_REQUIRED and rounds_crawled < MAX_CRAWL_ROUNDS:
        print(f"Got only {len(similar_sentences)} similar sentences. Crawling round {rounds_crawled + 1} started")
        new_data = []
        for crawler in crawlers:
            new_data += crawler.get_new_results(keywords=input_keywords, num_posts_to_crawl=POSTS_PER_ROUND)

        # Preprocess data class Sentence()
        n_data = preprocess(new_data)

        # Get top n of similar sentences
        n_similar_sentences, n_similarity_scores, n_similar_sentence_embeddings = \
            get_similar_sentences(input_string,
                                  n_data,
                                  minimum_similarity=MIN_SENTENCE_SIMILARITY,
                                  n_max=None,
                                  verbose=False)

        # Save the newly created embeddings
        sentence_sources = [similar_sentence.containing_post for similar_sentence in n_similar_sentences]
        sentences = [similar_sentence.content for similar_sentence in n_similar_sentences]
        print(sentences)
        EmbeddedSentence.add_sentences(sentences=sentences,
                                       embeddings=n_similar_sentence_embeddings,
                                       containing_posts=sentence_sources)
                                       # global_keywords=input_keywords

        # Append those embeddings to the similar sentences list
        similar_sentences = np.append(similar_sentences, n_similar_sentences)
        similarity_scores = np.append(similarity_scores, n_similarity_scores)

        content_list = [similar_sentence.content for similar_sentence in similar_sentences]
        unique_indices = np.array([content_list.index(single_content) for single_content in set(content_list)])
        if len(unique_indices) > 0:
            similar_sentences = similar_sentences[unique_indices]
            similarity_scores = similarity_scores[unique_indices]
        rounds_crawled += 1

    if rounds_crawled == MAX_CRAWL_ROUNDS:
        print("WE NEED MORE DATA, crawl some more in spare time!")  # TODO: this

    print(f"Data retrieval finished:\nCrawled a total of {rounds_crawled} rounds."
          f"\nFound a total of {len(similar_sentences)} relevant sentences.")

    for index, sentence in enumerate(similar_sentences):
        print(f"\n{sentence.content}, similarity: {similarity_scores[index]}")

    return similar_sentences, similarity_scores

    #
    # # Ensure there is some data to work with
    # no_new_data = new_data is None or len(new_data) == 0
    # no_cached_data = cached_sentences is None or len(cached_sentences) == 0
    #
    # if no_new_data and no_cached_data:
    #     raise ValueError("This method needs either new_data or cached_sentences to work with!")
    #
    # # TODO maybe we can use the class used in the database model (ScrapedPost)
    # if not no_new_data:
    #     # Process new data
    #     # Preprocess data class Sentence()
    #     n_data = nlp.preprocessing.preprocess(new_data)
    #
    #     # print("##### Preprocessed #####")
    #     # print(data)
    #     # print()
    #
    #     # Get top n of similar texts
    #     minimum_similarity = 0.5
    #     n_similar_sentences, n_similarity_scores, n_similar_sentence_embeddings = \
    #         nlp.clustering.get_similar_sentences(input_sentence,
    #                                              n_data,
    #                                              minimum_similarity=minimum_similarity,
    #                                              n_max=None,
    #                                              verbose=False)
    #
    #     # Save the newly created embeddings
    #     sentence_sources = [similar_sentence.containing_post for similar_sentence in n_similar_sentences]
    #     sentences = [similar_sentence.content for similar_sentence in n_similar_sentences]
    #     EmbeddedSentence.add_sentences(sentences=sentences,
    #                                    embeddings=n_similar_sentence_embeddings,
    #                                    containing_posts=sentence_sources,
    #                                    global_keywords=nlp.preprocessing.get_hotwords(input_sentence, -1))
    #
    #     similar_sentences = np.append(similar_sentences, n_similar_sentences)
    #     similarity_scores = np.append(similarity_scores, n_similarity_scores)
    #
    # # Process cached data
    #
    # if not no_cached_data:
    #     # Preprocess data class Sentence()
    #     c_data = [sentence.to_nlp_sentence() for sentence in cached_sentences]
    #
    #     # Get top n of similar texts
    #     minimum_similarity = 0.5
    #     c_similar_sentences, c_similarity_scores = \
    #         nlp.clustering.get_similar_cached_sentences(input_sentence,
    #                                                     cached_data=c_data,
    #                                                     cached_sentences=cached_sentences,
    #                                                     minimum_similarity=minimum_similarity,
    #                                                     n_max=None,
    #                                                     verbose=False)
    #
    #     similar_sentences = np.append(similar_sentences, c_similar_sentences)
    #     similarity_scores = np.append(similarity_scores, c_similarity_scores)
    #
    #
    # if relevant_sentences.count() < MIN_SENTENCES_REQUIRED:
    #     # Crawl some more stuff online
    #     print(f"Only {relevant_sentences.count()} cached sentences found. Let's get crawling!")
    #     return crawl_kidney_org(input_keywords), relevant_sentences
    # else:
    #     # Just return the cached sentences
    #     print(f"Cached sentences ({relevant_sentences.count()}) suffice!")
    #     return (), np.array(list(relevant_sentences))
